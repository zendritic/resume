{
    "$schema": "https://raw.githubusercontent.com/jsonresume/resume-schema/v1.0.0/schema.json",
    "basics": {
        "name": "Zackary Brewer",
        "label": "Network Automation Engineer",
        "email": "resume@zendritic.net",
        "url": "https://www.zendritic.net",
        "summary": "Highly skilled and motivated Infrastructure Automation Engineer with a strong background in network engineering. Leveraging 5 years of experience in developing infrastructure automation solutions for network devices using Ansible and Python. Possessing a Cisco CCNA certification and Cisco DevNet Associate certification. Committed to streamlining operations, improving efficiency, and optimizing network performance through automation. Proven track record in successfully implementing automation projects and driving impactful results. Seeking a challenging role to contribute technical expertise and drive innovation in infrastructure automation.",
        "location": {
            "countryCode": "US",
            "region": "Wisconsin",
            "city": "Madison"
        },
        "profiles": [
            {
                "network": "LinkedIn",
                "username": "zackarybrewer",
                "url": "https://www.linkedin.com/in/zackarybrewer/"
            }
        ]
    },
    "work": [
        {
            "name": "Epic Systems, Inc., Hosting",
            "position": "Network Development Lead",
            "startDate": "2019-02",
            "summary": "Led network automation effort by creating and presenting onboarding curricula to educate team members on automation methods and tools.",
            "highlights": [
                "Worked with other Development Leads across the organization to set and enforce best practices around review, testing, language standards, version control, and project management.",
                "Researched, designed, and developed an automation framework for managing complex infrastructure using Ansible, Go, Python, and other assorted tools.",
                "Developed Gitlab CI/CD pipelines for automation tools and related code.",
                "Integrated in-house automation tools with vendor tools such as Infoblox, ServiceNow, and PRTG.",
                "Administered virtualized and containerized systems and infrastructure."
            ],
            "url": "https://www.linkedin.com/company/epic1979/"
        },
        {
            "name": "Epic Systems, Inc., Hosting",
            "position": "Infrastructure Engineer",
            "startDate": "2018-02-28",
            "summary": "Supported multi-tenant, enterprise, data center, and WAN networks.",
            "highlights": [
                "Designed, implemented, and supported network solutions in a multi-tenant private cloud.",
                "Supported multiple customer environments including standard and unique LAN, WAN, and Firewall solutions.",
                "Responded to critical issues during business hours and on-call.",
                "Defined processes and wrote documentation for issue response, configuration, design, and training.",
                "Physically deployed hardware in data centers and at customer locations."
            ],
            "url": "https://www.linkedin.com/company/epic1979/"
        },
        {
            "name": "SupraNet Communications, Inc",
            "position": "Network Technician",
            "startDate": "2016-01-31",
            "endDate": "2018-02-28",
            "summary": "Configured and troubleshot Cisco, Juniper, Mikrotik, etc. routers, switches, and firewalls, to maintain ISP core systems, customer Internet service, and as consulting projects for small-business networks.",
            "url": "https://www.linkedin.com/company/supranet-communications-inc/"
        },
        {
            "name": "SupraNet Communications, Inc.",
            "position": "Internet Support Technician",
            "startDate": "2014-11-30",
            "endDate": "2016-01-31",
            "highlights": [
                "Unix Systems Administration for shared email and web hosting.",
                "Windows Active Directory desktop support and systems administration.",
                "Tier 1 NOC support for business Internet service and transport."
            ],
            "url": "https://www.linkedin.com/company/supranet-communications-inc/"
        },
        {
            "name": "California Parking Company",
            "position": "Technical Support/Project Manager",
            "startDate": "2008-10-31",
            "endDate": "2013-12-31",
            "highlights": [
                "Oversaw various projects, including the development of a new website.",
                "Researched, acquired, and installed equipment, software, and services.",
                "Managed sensitive, confidential, and proprietary information.",
                "Diagnosed and corrected problems with software and hardware.",
                "Improved user efficiency and data security through education and innovation.",
                "Communicated with clients, vendors, business partners, and employees."
            ],
            "url": "https://www.linkedin.com/company/california-parking-company/"
        }
    ],
    "volunteer": [
        {
            "position": "Secretary",
            "organization": "Eken Park Neighborhood Association (now North Street Neighborhood Ass.)",
            "startDate": "2020-01-01",
            "endDate": "2022-12-31"
        }
    ],
    "education": [
        {
            "institution": "Madison Area Technical College",
            "area": "Network Specialist",
            "studyType": "Associates of Science",
            "endDate": "2016-06"
        },
        {
            "institution": "Humboldt State University",
            "area": "Psychology",
            "studyType": "BA",
            "startDate": "2002-12",
            "endDate": "2007-12"
        }
    ],
    "certificates": [
        {
            "name": "Cisco Certified Network Associate Routing and Switching (CCNA)",
            "issuer": "Cisco",
            "endDate": "2025-01-31",
            "startDate": "2018-07-31",
            "url": "https://www.credly.com/badges/9b15273c-3cd2-4917-8ad3-be1710d576e3"
        },
        {
            "name": "Cisco Certified DevNet Associate",
            "issuer": "Cisco",
            "endDate": "2025-01-31",
            "startDate": "2022-01-31",
            "url": "https://www.credly.com/badges/8f4b345c-9024-4e0a-bdbf-45bc4915cc30"
        },
        {
            "name": "Understanding of Cisco Network Devices",
            "issuer": "Cisco",
            "url": "https://www.credly.com/badges/cfb565cc-61ae-4ee4-9615-80c6c0030e0a"
        }
    ],
    "skills": [
        {
            "name": "Network Engineering",
            "level": "Advanced",
            "keywords": [
                "BGP",
                "OSPF",
                "IS-IS",
                "Multiprotocol Label Switching (MPLS)",
                "VRF",
                "VXLAN",
                "IPv6",
                "IOS-XE",
                "IOS-XR",
                "JunOS",
                "NXOS/Nexus"
            ]
        },
        {
            "name": "Site Reliability Engineering",
            "level": "Intermediate",
            "keywords": [
                "Kubernetes",
                "Helm",
                "Flux",
                "Docker",
                "Splunk",
                "ELK (Elasticsearch)",
                "PRTG",
                "DNS",
                "PostgreSQL",
                "High Availability",
                "Disaster Recovery",
                "Scalability",
                "Capacity Planning",
                "Performance Tuning",
                "DevOps"
            ]
        },
        {
            "name": "Continuous Integration and Continuous Delivery (CI/CD)",
            "level": "Advanced",
            "keywords": [
                "GitLab Auto DevOps",
                "Pipelines",
                "Building",
                "Packaging",
                "Deployment",
                "Unit Testing",
                "Integration Testing",
                "Test Driven Development"
            ]
        },
        {
            "name": "Infrastructure Automation",
            "level": "Expert",
            "keywords": [
                "Ansible",
                "Puppet",
                "Terraform",
                "Facts",
                "Inventories"
            ]
        },
        {
            "name": "Ansible Tower (AAP/AWX)",
            "level": "Advanced",
            "keywords": [
                "Execution Environments",
                "Kubernetes (OpenShift)",
                "Operator",
                "AWXKit",
                "Collections"
            ]
        },
        {
            "name": "Software Development",
            "level": "Intermediate",
            "keywords": [
                "Python",
                "Go (Golang)",
                "Shell (Bash) Scripting",
                "PowerShell"
            ]
        },
        {
            "name": "Systems Administration",
            "level": "Advanced",
            "keywords": [
                "Linux",
                "Debian",
                "Red Hat Enterprise Linux",
                "Patching"
            ]
        },
        {
            "name": "Network Security",
            "level": "Advanced",
            "keywords": [
                "Microsegmentation",
                "Next Generation Firewall",
                "Encryption",
                "Certificates",
                "Packet Inspection"
            ]
        },
        {
            "name": "Technical Support",
            "level": "Advanced",
            "keywords": [
                "RCA",
                "OSI Model",
                "Communication",
                "Troubleshooting",
                "Triage",
                "On-call"
            ]
        },
        {
            "name": "Communication",
            "level": "Advanced",
            "keywords": [
                "Written",
                "Verbal",
                "Customer Facing",
                "Team Leadership",
                "Mentoring",
                "Efficient Documentation"
            ]
        },
        {
            "name": "Project Management",
            "level": "Intermediate",
            "keywords": [
                "Epic",
                "Iteration",
                "Milestone",
                "Sprint",
                "Issue Board (Kanban)",
                "Application Design",
                "Scrum",
                "Agile"
            ]
        }
    ],
    "languages": [
        {
            "language": "English",
            "fluency": "Native Speaker"
        },
        {
            "language": "Spanish",
            "fluency": "Intermediate"
        }
    ],
    "interests": [
        {
            "name": "Photography"
        },
        {
            "name": "Zymurgy"
        }
    ],
    "meta": {
        "version": "v1.0.0",
        "canonical": "https://github.com/jsonresume/resume-schema/blob/v1.0.0/schema.json"
    }
}