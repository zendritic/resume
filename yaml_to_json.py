import yaml
import json
import argparse
import os

def get_unique_file_path(file_path):
        """
        Generate a unique file path by appending an incrementing suffix if the file already exists.

        Parameters:
        file_path (str): The original file path.

        Returns:
        str: A unique file path.
        """
        base, extension = os.path.splitext(file_path)
        counter = 1
        new_file_path = file_path

        while os.path.exists(new_file_path):
            new_file_path = f"{base}_{counter}{extension}"
            counter += 1
        
        return new_file_path

def yaml_to_json(yaml_file_path, json_file_path):
    """
    Convert a YAML file to a JSON file.

    Parameters:
    yaml_file_path (str): Path to the input YAML file.
    json_file_path (str): Path to the output JSON file.

    Returns:
    None
    """
    try:
        # Open the YAML file and load its content
        with open(yaml_file_path, 'r') as yaml_file:
            yaml_content = yaml.safe_load(yaml_file)

        # Convert the content to JSON format and write to the JSON file
        with open(json_file_path, 'w') as json_file:
            json.dump(yaml_content, json_file, indent=4)
        
        print(f"Successfully converted {yaml_file_path} to {json_file_path}")
    
    except Exception as e:
        print(f"Error: {e}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert a YAML file to a JSON file.')
    parser.add_argument('yaml_file_path', type=str, nargs='?', help='Path to the input YAML file.')
    parser.add_argument('json_file_path', type=str, nargs='?', help='Path to the output JSON file.')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s 1.0')

    args = parser.parse_args()

    if not args.yaml_file_path and not args.json_file_path:
        parser.print_help()
    
    if args.json_file_path is None:
        base, _ = os.path.splitext(args.yaml_file_path)
        args.json_file_path = base + '.json'
    
    else:
        yaml_to_json(args.yaml_file_path, args.json_file_path)